from django.urls import path
from reciepts.views import (
    RecieptListView,
    AccountCreateView,
    CategoryCreateView,
    RecieptCreateView,
    AccountListView,
    ExpenseCategoryListView,
)

urlpatterns = [
    path("", RecieptListView.as_view(), name="home"),
    path(
        "accounts/create/", AccountCreateView.as_view(), name="create_account"
    ),
    path(
        "categories/create",
        CategoryCreateView.as_view(),
        name="create_category",
    ),
    path("create", RecieptCreateView.as_view(), name="create_reciept"),
    path("accounts/", AccountListView.as_view(), name="account_list"),
    path(
        "categories/", ExpenseCategoryListView.as_view(), name="category_list"
    ),
]
